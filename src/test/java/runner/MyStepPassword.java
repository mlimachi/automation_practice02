package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import page.*;

public class MyStepPassword {
    // instanciar nuestras clases
    MenuSection menuSection = new MenuSection();
    SettingsModal settingsModal = new SettingsModal();

    @When("realizo clic en enlace Setting")
    public void realizoClicEnEnlaceSetting() {
        menuSection.settingsButton.click();
    }

    @And("escribo mi contrasena antigua {string}")
    public void escriboMiContrasenaAntigua(String oldPassword) {
        settingsModal.oldpasswordTxtBox.writeText(oldPassword);
    }

    @And("escribo mi contrasena nueva {string}")
    public void escriboMiContrasenaNueva(String newPassword) {
        settingsModal.newpasswordTxtBox.writeText(newPassword);
    }

    @And("realizo clic en el boton ok")
    public void realizoClicEnElBotonOk() {
        settingsModal.okButton.click();
    }
}
