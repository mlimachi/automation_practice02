package runner;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.LeftSection;

public class MyStepProject {
    // instanciar nuestra clase LeftSection
    LeftSection leftSection = new LeftSection();
    @When("yo creo un proyecto con el nombre {string}")
    public void yoCreoUnProyectoConElNombre(String nameProject) {
        leftSection.addNewProjectButton.click();
        leftSection.newProjectTextBox.writeText(nameProject);
        leftSection.addButton.click();
    }

    @Then("el proyecto {string} deberia ser mostrado")
    public void elProyectoDeberiaSerMostrado(String nameProject) {
        Assertions.assertTrue(leftSection.isNameProjectDisplayed(nameProject),
                "ERROR. El proyecto no es mostrado.");
    }

    @When("yo actualizo el proyecto {string} con el nombre {string}")
    public void yoActualizoElProyectoConElNombre(String nameProjectOld, String nameProjectNew) {
        leftSection.clickNameProject(nameProjectOld);
        leftSection.menuButton.click();
        leftSection.editButton.click();
        leftSection.editProjectTextBox.writeText(nameProjectNew);
        leftSection.saveButton.click();
    }
}
