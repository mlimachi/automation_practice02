package runner;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import page.*;

import java.util.Map;

public class MyStepAccount {
    // instanciar nuestras clases
    MainPage mainPage = new MainPage();
    LoginModal loginModal = new LoginModal();
    MenuSection menuSection = new MenuSection();
    SignupModal signupModal = new SignupModal();

    // generar un numero aleatorio entre 1 y 10 para las pruebas
    int numeroRandom = (int) (1 + Math.random()*10);

    @When("realizo clic en el boton SignUpFree")
    public void realizoClicEnElBotonSignUpFree() {
        mainPage.signupButton.click();
    }

    @And("escribo mi nombre {string}")
    public void escriboMiNombre(String name) {
        signupModal.fullnameTxtBox.writeText(name);
    }

    @And("escribo mi correo2 {string}")
    public void escriboMiCorreo(String email) {
        signupModal.emailTxtBox.writeText("correo" + numeroRandom + email);
    }

    @And("escribo mi password {string}")
    public void escriboMiPassword(String password) {
        signupModal.passwordTxtBox.writeText(password);
    }

    @And("acepto las condiciones del servicio")
    public void aceptoLasCondicionesDelServicio() {
        signupModal.acceptCheck.click();
    }

    @And("realizo clic en el boton SignUp")
    public void realizoClicEnElBotonSignUp() throws InterruptedException {
        signupModal.signupButton.click();

        // esperar
        Thread.sleep(3000);
    }

    // --------------------------------------------------
    @When("realizo clic en enlace Logout")
    public void realizoClicEnEnlaceLogout() {
        menuSection.logoutButton.click();
    }

    @Then("deberia salir de la aplicacion")
    public void deberiaSalirDeLaAplicacion() {
        Assertions.assertTrue(mainPage.loginButton.isControlDisplayed(),
                "ERROR. No se pudo realizar el Logout.");
    }

    @When("yo quiero realizar el login2")
    public void yoQuieroRealizarElLogin2(Map<String, String> credential) {
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText("correo" + numeroRandom +credential.get("email"));
        loginModal.pwdTxtBox.writeText(credential.get("password"));
        loginModal.loginButton.click();
    }
}
