package runner;

import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;
import java.util.Map;

public class MyStepdefs {
    @Given("tengo acceso a {}")
    public void tengoAccesoATodoLy(String app) {
    }

    @When("realizo clic en el boton login")
    public void realizoClicEnElBotonLogin() {
    }

    @And("ingreso mi correo electronico: {string}")
    public void ingresoMiCorreoElectronicoMicorreoCorreoCom(String correo) {
    }

    @And("ingreso mi password: {int}")
    public void ingresoMiPassword(int arg0) {
    }

    @Then("deberia ingresar a la aplicacion")
    public void deberiaIngresarALaAplicacion() {
    }

    @And("los siguientes controles deberian ser mostrados")
    public void losSiguientesControlesDeberianSerMostrados(List<String> controls) {
        for (String control: controls) {
            System.out.println("controls: " + control);
        }
    }

    @And("ingreso a la aplicacion usando")
    public void ingresoALaAplicacionUsando(Map<String,String> credential) {
        for (String control: credential.keySet()) {
            System.out.println(control + " tiene el valor "+ credential.get(control));
        }
    }

    //-------------------- 2 metodos para Feature de tipo OBJETOS

    @And("me registro a la pagina")
    public void meRegistroALaPagina(Registro registro) {
        System.out.println("Name: " + registro.getName());
        System.out.println("Phone: " + registro.getPhone());
        System.out.println("Address: " + registro.getAddress());
        System.out.println("CI: " + registro.getCi());
    }

    // convertir los datos de una tabla a un objeto con registros
    @DataTableType
    public Registro convert(Map<String, String> entity) {
        Registro registro = new Registro();
        registro.setName(entity.get("name"))
                .setPhone(entity.get("phone"))
                .setAddress(entity.get("address"))
                .setCi(entity.get("ci"));
        return registro;
    }
}
