package session;

import browser.FactoryBrowser;
import org.openqa.selenium.WebDriver;
/*
TÉCNICA/PATRON: SINGLETON
La instancia que se cree, debe ser única
Caracteristicas:
+ tener un objeto del mismo tipo como atributo
+ tener el constructor privado
+ tener un método estático público que controle/retorne la instancia
*/
public class Session {
    // tener un objeto del mismo tipo como atributo
    private static Session session = null;

    // navegador
    private WebDriver browser;

    // tener el constructor privado
    private Session () {
        // crear el navegador con el paquete browser
        browser = FactoryBrowser.make("chrome").create();
    }

    // tener un método estático público que controle/retorne la instancia
    public static Session getInstance() {
        if (session == null)
            session = new Session();
        return session;
    }
    // en este método, no usar synchronise.. porque no permite realizar pruebas paralelasa
    // public synchronized Session getInstance() { ... }

    // cerrar la sesion
    public void closeSession() {
        browser.quit();     // cerrar el navegador
        session = null;     // cerrar la sesion
    }

    // obtener el navegador
    public WebDriver getBrowser() {
        return browser;
    }
}
