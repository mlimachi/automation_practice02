package control;

import org.openqa.selenium.By;

// hereda de nuestra clase Control
public class TextBox extends Control{
    public TextBox(By locator) {
        super(locator);
    }

    // accion: escribir texto
    public void writeText (String value) {
        findControl();              // encontrar el control
        control.clear();            // limpiar el contenido
        control.sendKeys(value);    // enviar el valor al control
    }
}
