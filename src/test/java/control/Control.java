package control;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import session.Session;

/*
En Selenium, cualquier "control" es un elemento web
los maneja mediante localizadores
 */
public class Control {
    protected WebElement control;   // control
    protected By locator;           // localizador

    // constructor: pedir el localizador
    public Control (By locator) {
        this.locator = locator;
    }

    // buscar un control
    protected void findControl () {
        // buscar en la sesion --> instancia --> navegador --> elemento
        control = Session.getInstance().getBrowser().findElement(locator);
    }

    // accion: clic
    public void click() {
        this.findControl();
        control.click();
    }

    // accion: es desplegado
    public boolean isControlDisplayed() {
        try {
            this.findControl();
            return control.isDisplayed();
        }catch (Exception e) {
            return false;
        }
    }
}
