package page;

import control.Button;
import control.TextBox;
import org.openqa.selenium.By;

public class MenuSection {
    // controles del menu
    public Button settingsButton = new Button(By.xpath("//a[@href='javascript:OpenSettingsDialog();']"));
    public Button logoutButton = new Button(By.id("ctl00_HeaderTopControl1_LinkButtonLogout"));
}
