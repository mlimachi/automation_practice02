package testUI;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import session.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import page.LoginModal;
import page.MainPage;
import page.MenuSection;

public class LoginTest {
    // controles
    MainPage mainPage = new MainPage();
    LoginModal loginModal = new LoginModal();
    MenuSection menuSection = new MenuSection();

    @BeforeEach
    public void goWebApp() {
        // ir a la aplicacion
        Session.getInstance().getBrowser().get("http://todo.ly/");
    }

    @AfterEach
    public void closeWebApp() {
        // cerrar la sesion
        Session.getInstance().closeSession();
    }

    @Test
    public void verifyLoginIsSuccessfully() {
        // en página principal, en el botón login, hacer un clic
        // en el textbox escribir texto
        // clic en boton login
        // verificar si la página principal es mostrada
        mainPage.loginButton.click();
        loginModal.emailTxtBox.writeText("maestria@maestria.com");
        loginModal.pwdTxtBox.writeText("12345");
        loginModal.loginButton.click();

        Assertions.assertTrue(menuSection.logoutButton.isControlDisplayed(),
                "ERROR. El login falló.");
    }
}
