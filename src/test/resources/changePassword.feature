Feature: ChangePassword

  @RegressionPassword
  Scenario: change the password
    Given la pagina "http://todo.ly" este abierta
    When yo quiero realizar el login
      | email    | correo@correo.com |
      | password | 12345             |
    Then yo deberia ingresar a la app web

    When realizo clic en enlace Setting
    And escribo mi contrasena antigua "12345"
    And escribo mi contrasena nueva "55555"
    And realizo clic en el boton ok
    Then deberia ingresar a la aplicacion

    When realizo clic en enlace Logout
    Then deberia salir de la aplicacion

    When yo quiero realizar el login
      | email    | correo@correo.com |
      | password | 55555             |
    Then yo deberia ingresar a la app web
