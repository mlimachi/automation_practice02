Feature: NewAccount

  @RegressionAccount
  Scenario: create a new account
    Given la pagina "http://todo.ly" este abierta
    When realizo clic en el boton SignUpFree
    And escribo mi nombre "juan"
    And escribo mi correo2 "@correo.com"
    And escribo mi password "12345"
    And acepto las condiciones del servicio
    And realizo clic en el boton SignUp
    Then deberia ingresar a la aplicacion

    When realizo clic en enlace Logout
    Then deberia salir de la aplicacion

    When yo quiero realizar el login2
      | email    | @correo.com |
      | password | 12345       |
    Then deberia ingresar a la aplicacion

