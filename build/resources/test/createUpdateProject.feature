Feature: Project

  @Regression
  Scenario: verify the create/update project

    Given la pagina "http://todo.ly" este abierta
    And yo quiero realizar el login
      | email    | maestria@maestria.com |
      | password | 12345                 |
    When yo creo un proyecto con el nombre "Cucumber11"
    Then el proyecto "Cucumber11" deberia ser mostrado
    When yo actualizo el proyecto "Cucumber11" con el nombre "CucumberUpdate11"
    Then el proyecto "CucumberUpdate11" deberia ser mostrado
